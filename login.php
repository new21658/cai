<?php

  $username = "";
  $password = "";

  $username_err = "";
  $password_err = "";
  $all_err = "";


  if($_SERVER["REQUEST_METHOD"] == "POST") {

    $username = $_POST["username"];
    $password = $_POST["password"];

    $username = validateLogin($username);
    $password = validateLogin($password);

    if(empty($username)) {
      $username_err = "Username must not empty";
    }

    else if(empty($password)) {
      $password_err = "Password must not empty";
    }

    else {
      require_once("db.php");

      try{
        $db = get_db_connect();
        $stm = $db->prepare("SELECT * FROM cai_user WHERE username=:username AND password=:password;");
        $stm->bindParam(":username", $username);
        $stm->bindParam(":password", $password);
        $stm->execute();
        $rs = $stm->fetchAll(PDO::FETCH_ASSOC);

        if(count($rs) > 0) {
          // SET SESSION
          $_SESSION["cai_user"] = $rs[0]['username'];
          require_once("dashboard.php");
          exit();
        }
        else {
          $all_err = "Username or Password Invalid";
        }

        $stm = null;
        $db = null;

      }
      catch(PDOException $e) {
          echo "Database Error: ".$e->getMessage();
      }
    }


}


  function validateLogin($data) {
    $data = trim($data);
    $data = htmlspecialchars_decode($data);
    return $data;
  }


 ?>



<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CAI</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link rel="stylesheet" type="text/css" href="css/login.css" />
  </head>
  <body>
    <div class="login-root">
        <div class="login-avatar">
          <img src="imgs/avatar1.png" />
        </div>
        <h3><center>CAI ปรค.57-2</center></h3>
        <form name="login" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" >
            <input type="text" name="username" required  placeholder="Username"/>
              <label style="color: red"><?php echo $username_err; ?></label>
            <input type="password" name="password" required  placeholder="Password"/>
              <label style="color: red"><?php echo $password_err; ?></label>
            <input type="submit" class="bt-m" value="Login"/><hr/>
              <label style="color: red"><?php echo $all_err; ?></label>
        </form>
    </div>
  </body>
</html>
