<?php
  if(empty($username) || !isset($username)) {
    header("Location:  login.php");
    exit();
  }
  require_once("db.php");
  $db = get_db_connect();
  $rs;
  $message_err = "";
  $countRow;

  try{

    $sql = "SELECT * FROM cai_marks WHERE username=:username";
    $stm = $db->prepare($sql);
    $stm->bindParam(":username", $username);
    $stm->execute();
    $rs = $stm->fetchAll(PDO::FETCH_ASSOC);
    $countRow = count($rs);

    if($countRow < 1) {
      $message_err = "ไม่พบคะแนนผู้เรียน";
    }

  }
  catch(PDOException $e) {
    echo "Database Error : ".$e->getMessage();
  }

 ?>

 <!DOCTYPE html>
 <html>
 <head>
   <title><?php echo "$username";?></title>
   <link rel="stylesheet" type="text/css" href="css/main.css" />
   <link rel="stylesheet" type="text/css" href="css/dashboard.css" />
 </head>
 <body>
   <div class="dashb-root">
    <?php
      if(!empty($message_err)) {
        echo "<h1>$message_err</h1>";
      }
    ?>
    <h1>บัญชีของคุณ <?php  echo $username; ?></h1>
    <h2>ตารางคะแนนของผู้เรียน</h2>
    <!-- Table Of Marks  -->
     <table class="dashb-table">
       <thead>
         <tr>
           <th>ชื่อผู้เรียน</th>
           <th>ชื่อวิชา</th>
           <th>บทที่ 1 สอบก่อนเรียน</th>
           <th>บทที่ 1 สอบหลังเรียน</th>
           <th>บทที่ 2 สอบก่อนเรียน</th>
           <th>บทที่ 2 สอบหลังเรียน</th>
           <th>บทที่ 3 สอบก่อนเรียน</th>
           <th>บทที่ 3 สอบหลังเรียน</th>
         </tr>
       </thead>
       <tbody>
         <?php
            foreach($rs as $row) {

              $l1_pre = $row['l1_pretest_mark'];
              $l1_post = $row['l1_posttest_mark'];
              $l1_avg = $l1_post - $l1_pre;

              $l2_pre = $row['l2_pretest_mark'];
              $l2_post = $row['l2_posttest_mark'];
              $l2_avg = $l2_post - $l2_pre;

              $l3_pre = $row['l3_pretest_mark'];
              $l3_post = $row['l3_posttest_mark'];
              $l3_avg = $l3_post - $l3_pre;

              $green_hl = "background-color: rgba(51, 204, 51, 0.5)";
              $red_hl = "background-color: rgba(255, 80, 80, 0.5)";

              $style_hl_1 = $red_hl;
              $style_hl_2 = $red_hl;
              $style_hl_3 = $red_hl;

              if($l1_avg > 0) {
                $style_hl_1 = $green_hl;
              }
              if($l2_avg > 0) {
                $style_hl_2 = $green_hl;
              }
              if($l3_avg > 0) {
                $style_hl_3 = $green_hl;
              }

              echo "<tr>";
                echo "<td>".$row['learner_name']."</td>";
                echo "<td>".$row['subject_name']."</td>";
                echo "<td>".$row['l1_pretest_mark']."</td>";
                echo "<td>".$row['l1_posttest_mark']."</td>";
                echo "<td>".$row['l2_pretest_mark']."</td>";
                echo "<td>".$row['l2_posttest_mark']."</td>";
                echo "<td>".$row['l3_pretest_mark']."</td>";
                echo "<td>".$row['l3_posttest_mark']."</td>";
              echo "</tr>";
              // echo "<tr>";
              //   echo "<td colspan='3'>ส่วนต่างคะแนน</td>";
              //   echo "<td colspan='2' style='".$style_hl_1."'><center> $l1_avg  </center></td>";
              //   echo "<td colspan='2' style='".$style_hl_2."'><center> $l2_avg  </center></td>";
              //   echo "<td colspan='2' style='".$style_hl_3."'><center> $l3_avg  </center></td>";
              //   echo "<td></td>";
              // echo "</tr>";
            }
         ?>
       </tbody>
     </table>
     <hr/>
     <a href="logout.php" class="a-link">ออกจากระบบ</a>
   </div>
 </body>
 </html>
