
function getAjax() {

  var xhttp;
  if (window.XMLHttpRequest) {
      xhttp = new XMLHttpRequest();
      } else {
      // code for IE6, IE5
      xhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }

  return xhttp;

}

// Send Loops ================================================================
function sendMark(data_send) {

  var ajax = getAjax();

  xhttp.open("POST", "http://104.199.248.242/cai/save_marks.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("data_send="+JSON.stringify(data_send));

  xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      var res = this.responseText;
      if(res == "marks inserted successful!") {
        console.log("ส่งคะแนนสำเร็จ!");
      } else {
        console.log(res);
      }
    }
  }
}
// end send Loops =============================================================

function randMarks(min, max) {
  return min + Math.round(Math.random()*max);
}

// main

var learner_list = [
  "เกศินี กนิษฐจินดา",
  "อภินันท์ สิทธิพัฒน์สกุล",
  "จุฑามาศ พุ่มสงวน",
  "ธีรรัตน์ แสงวิสูตร",
  "ภัทรวดี สร้อยมณี",
  "ชัยกาญจน์ เติมสินทวีสุข",
  "PREECHA TEPAKDEE",
  "สายธาร เถียรหนู",
  "ปารมี อู่ทอง",
  "กาญจนา คงดอน",
  "พนิตา ยังสุข",
  "MONGKHON KATASEELA",
  "SIRIRAK LIAJU",
  "ณัฐพล สิงห์อุดม",
  "พิชชาภา อมตวีระกุล",
  "TEERAPONG PINIDKAN",
  "นิติพร นุ่มเฉย",
  "กาญจนา เอี่ยมภิญโญ",
  "ORAPAN SOOKPRASIT",
  "วิลาสินี ตันวงศ์เลิศ",
  "ธีรรัตน์ แสงวิสูตร",
  "NUNTIYA POMIN",
  "จุฑามาศ พุ่มสงวน",
  "อริสรา ชายวิชัย",
  "ธิดารัตน์ ศรีระยับ",
  "นภัสสร บุญมานะ",
  "ORAWAN MACHARERN",
  "VASIN CHAIWAN",
  "วีรภัทร หลีระพงศ์",
  "รินทริยา บรรพหาร"
];

function sendMarkLoops(learner_name, subject) {
  var data_send = {};
  var subject = subject;
  var marks = {};
  var learner = learner_name;

  marks["l1_pretest_mark"] = randMarks(0, 50);
  marks["l1_posttest_mark"] = randMarks(5, 95);
  marks["l2_pretest_mark"] = randMarks(0, 50);
  marks["l2_posttest_mark"] = randMarks(7, 70);
  marks["l3_pretest_mark"] = randMarks(0, 30);;
  marks["l3_posttest_mark"] = randMarks(5, 80);

  data_send.subject = subject;
  data_send.learner = learner;
  data_send.marks = marks;

  sendMark(data_send);

}

//sendMarkLoops("ภูวเนตร เวชภูติ");
