CREATE DATABASE cai;

CREATE TABLE cai_user (
  username varchar(100) primary key,
  password varchar(30),
  subject_name varchar(30)
)   CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE cai_marks (
  username varchar(100),
  learner_no int auto_increment primary key,
  learner_name varchar(100),
  subject_name varchar(30),
  l1_pretest_mark int,
  l1_posttest_mark int,
  l2_pretest_mark int,
  l2_posttest_mark int,
  l3_pretest_mark int,
  l3_posttest_mark int
) CHARACTER SET utf8 COLLATE utf8_general_ci ;

INSERT INTO cai_user VALUES ("root", "1234", "oop");
INSERT INTO cai_user VALUES ("monpon745", "14568520*", "basic_com");
INSERT INTO cai_user VALUES ("nathawut", "12345678", "os");
INSERT INTO cai_user VALUES ("muttana", "12345678", "modern_cai");
INSERT INTO cai_user VALUES ("nungning", "12345678", "pascal");
INSERT INTO cai_user VALUES ("fbp", "12345678", "computerinformation");
INSERT INTO cai_user VALUES ("pleng", "12345678", "marketing");
INSERT INTO cai_user VALUES ("Network", "12345678", "data_com");
INSERT INTO cai_user VALUES ("priyapa", "12345678", "micro_economic");
INSERT INTO cai_user VALUES ("tawan", "12345678", "economics_cai");
INSERT INTO cai_user VALUES ("bos123", "12345678", "statistics");
INSERT INTO cai_user VALUES ("flim", "12345678", "ethics_computer");

INSERT INTO cai_marks (username, learner_name, subject_name,
  l1_pretest_mark,
  l1_posttest_mark,
  l2_pretest_mark,
  l2_posttest_mark,
  l3_pretest_mark,
  l3_posttest_mark
) VALUES (
  "root", "eiei", "oop",
  10, 20, 30, 40, 50, 60
);
