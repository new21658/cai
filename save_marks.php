<?php


  if($_SERVER["REQUEST_METHOD"] == "POST") {

// test
    //echo "this is save_marks";
    //exit();
// ---------
    $data_send = json_decode($_POST["data_send"]);

    $subject = $data_send->subject;
    $learner = $data_send->learner;
    $marks = $data_send->marks;
    $username;

    require_once("db.php");

    try{

          $db = get_db_connect();

          $sql = "SELECT username FROM cai_user WHERE subject_name=:subject";
          $stm = $db->prepare($sql);
          $stm->bindParam(":subject", $subject);
          $stm->execute();

          $rs = $stm->fetchAll(PDO::FETCH_ASSOC);
          if(count($rs) < 1) {
            echo "Error No Username match with Subject.";
            exit();
          }
          print_r($rs);
          $username = $rs["username"];

          $sql = "INSERT INTO cai_marks (username, learner_name, subject_name,
            l1_pretest_mark,
            l1_posttest_mark,
            l2_pretest_mark,
            l2_posttest_mark,
            l3_pretest_mark,
            l3_posttest_mark,
          ) VALUES (:username, :learner, :subject,
            :l1_pretest_mark,
            :l1_posttest_mark,
            :l2_pretest_mark,
            :l2_posttest_mark,
            :l3_pretest_mark,
            :l3_posttest_mark,
          )";

          $stm = $db->prepare($sql);

          $stm->bindParam(":username", $username);
          $stm->bindParam(":learner", $learner);
          $stm->bindParam(":subject", $subject);
          $stm->bindParam(":l1_pretest_mark", $marks->l1_pretest_mark);
          $stm->bindParam(":l1_posttest_mark", $marks->l1_posttest_mark);
          $stm->bindParam(":l2_pretest_mark", $marks->l2_pretest_mark);
          $stm->bindParam(":l2_posttest_mark", $marks->l2_posttest_mark);
          $stm->bindParam(":l3_pretest_mark", $marks->l3_pretest_mark);
          $stm->bindParam(":l3_posttest_mark", $marks->l3_posttest_mark);

          if($stm->execute()) {
            echo "marks inserted successful!";
          } else {
            echo "marks inserted fail.";
          }

          $stm = null;
          $db = null;
          exit();

    }
    catch(PDOException $e) {
      echo "Database Error :".$e->getMessage();
    }




  }

 ?>
