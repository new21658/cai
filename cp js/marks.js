
// Init First SLide
function init_cai() {

  if(window.sessionStorage) {
    // reset marks_sent
    window.sessionStorage.marks_sent = 0;

    var marksObj = {};
    marksObj["l1_pretest_mark"] = -1;
    marksObj["l1_posttest_mark"] = -1;
    marksObj["l2_pretest_mark"] = -1;
    marksObj["l2_posttest_mark"] = -1;
    marksObj["l3_pretest_mark"] = -1;
    marksObj["l3_posttest_mark"] = -1;

    var marksStr = JSON.stringify(marksObj);

    window.sessionStorage.subject = window.cpAPIInterface.getVariableValue("cpInfoProjectName");
    window.sessionStorage.learner = "no name";
    window.sessionStorage.marks = marksStr;
    // for check mark sent
    window.sessionStorage.marks_sent = 0;
  } else {
    console.log("Sory Your browser not support sessionStorage.");
  }

}

init_cai();
//===============================End Init=======================================

// save learner

function login_cai() {

  var learner = window.cpAPIInterface.getVariableValue("username");
  if(learner == "" || learner == undefined || learner == null){
    window.alert("กรุณาใส่ชื่อ");
    return;
  }
  if (typeof learner == "string") {
    if(learner.trim() == "") {
      window.alert("กรุณาใส่ชื่อ");
      return;
    }
  }
  window.sessionStorage.learner = learner;
  window.cpAPIInterface.next();
  window.setTimeout(function(){ window.cpAPIInterface.play(); }, 100);
  }
login_cai();

// ============================= End Save Leanrer =================================

// Send MArks To Server.
function sendMarks() {
  function getAjax() {

    var xhttp;
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
        } else {
        // code for IE6, IE5
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    return xhttp;

  }

  var data_send = {};

  var subject;
  var marks;
  var learner;

  if(window.sessionStorage) {
    // check IF marsk was sent to server
    if(window.sessionStorage.marks_sent == 1) {
      window.alert("คุณได้ส่งคะแนนไปแล้ว");
      return;
    }

     subject = window.sessionStorage.subject;
     marks = JSON.parse(window.sessionStorage.marks);
     learner = window.sessionStorage.learner;
  } else {
    console.log("sessionStorage Error");
    return;
  }

  for(key in marks) {
    if(marks[key] == "" || marks[key] < 0 || marks[key] == undefined) {
      if(marks[key] != 0) {
      window.alert("กรุณาทำข้อสอบให้ครบก่อนส่งคะแนนนะ");
      return;
    }
    }
  }

  data_send.subject = subject;
  data_send.learner = learner;
  data_send.marks = marks;

  var xhttp = getAjax();

  xhttp.open("POST", "http://104.199.248.242/cai/save_marks.php", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send("data_send="+JSON.stringify(data_send));

  xhttp.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) {
      var res = this.responseText;
      if(res == "marks inserted successful!") {
        window.alert("ส่งคะแนนสำเร็จ! ขอบคุณครับ");
        // set marks sent
        window.sessionStorage.marks_sent = 1;
      } else {
        window.alert(res);
      }
    }
  }
}
sendMarks();
// =============================== End Send Marks ==============================

// Save Marks To sessionStorage
// pattern l1_pretest_mark l1_posttest_mark
function saveMarksToSession(lesson, type) {
  var mark = window.cpAPIInterface.getVariableValue("cpQuizInfoPointsscored");
  console.log("mark score = "+mark);
  if(window.sessionStorage.marks) {
      var marksObj = JSON.parse(window.sessionStorage.marks);
      marksObj[lesson+"_"+type+"_mark"] = mark;
      var marksStr = JSON.stringify(marksObj);
      window.sessionStorage.marks = marksStr;
      console.log("marks save to session completed.");
  } else {
    console.log("sessionStorage.marks not defined.");
  }
}
saveMarksToSession();
//=============================== End saveMarksToSession =======================
